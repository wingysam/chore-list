const puppeteer = require('puppeteer')
const fs = require('fs').promises
const moment = require('moment')
const YAML = require('yaml')
const path = require('path')
const pug = require('pug')

async function main () {
  const config = YAML.parse((await fs.readFile('config.yml')).toString())

  // Compiler function https://pugjs.org/api/getting-started.html
  const renderTemplate = pug.compileFile('template.pug')

  // Generate the HTML and save it to disk
  await fs.writeFile('rendered.html', await renderTemplate({
    config,
    moment
  }))

  // Control Chromium to take a screenshot of the generated webpage
  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await page.goto(`file://${path.resolve('./rendered.html')}`)

  await page.pdf({ path: 'rendered.pdf', printBackground: true })

  await browser.close()

  // Clean up
  await fs.unlink('rendered.html')
}

main()
