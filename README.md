# Chore List
Create a PDF of a list of chores from a YAML configuration

![Screenshot of output PDF](media/output.png)

## Instructions
### Requirements
* Node 10.3.0 or higher
### Configure
Create a YAML config in `chores.yml`.

This should look something like this:
```yaml
person: Wingysam
chores:
  -
    text:
      - Sweep
      - Vacuum
    amount: 3
  -
    text: Garbage
    amount: 1
```
### Install
`npm i` or `yarn` or `pnpm i`
### Run
`node index.js`